

/**
 * Функция отслеживает выделение текста и записывает его в куки
 * для дальнейшей  отправки в службу проверки на орфографию и показа ошибок
 * */
    function get_selected_text() {
        if (window.getSelection()) {
            var select = window.getSelection();
            let textSelect = select.toString();
            if(textSelect) {
                $.cookie('select_text', textSelect);
            }

        }
    }


/**
* Функция навешивания событий на кнопки Вспывающего окошка
* */
    function clickOpenoknoTytoknoall() {

        $(".openokno").click(function () {
            $(".blockcentr").slideToggle("2000");
            if ($("div").hasClass("blockall"))
                $(".blockall").remove();
            else
                $(".tytoknoall").append('<div class="blockall"></div>');
        });
        $(".tytoknoall").click(function () {
            $(".blockcentr").slideToggle("2000");
            $(".blockall").remove();
        });

    }

/**
* Функция отправляет аякс запрос в сервис Яндекс для проверки орфографии
 *
 * */
    function ajaxYandexSpellchecking() {

        let textCookie = $.cookie('select_text');
        let url = "https://speller.yandex.net/services/spellservice.json/checkText";

        $.post(
            url,
            {
                "text" : textCookie
            },
            function(data){

                let textCookie = $.cookie('select_text');
                let textCookieNew = textCookie;
                let strCookie = textCookie.split(' ');

                if ($.isEmptyObject(data)) {
                    // console.log('пуст');
                } else {

                    if(data.length > 0) {
                        for(i=0; i<data.length; i++) {
                            textCookieNew = textCookieNew.replace(new RegExp(data[i]['word'],'g'), '<span style="color:green;">' + data[i]['s'][0] + '</span>' + ' <span style="color:red;text-decoration:line-through;">' + data[i]['word'] + '</span>');
                        }

                    };
                };

                //Кладем данные в всплывашку
                    $(".blockcentr").empty();
                    $(".blockcentr").append('<label class="openokno" style="cursor: pointer;float: right;color: green;">Закрыть</label><br/>');
                    $(".blockcentr").append('<div>' + textCookieNew + '</div>');

                //Навешиваем события кликов на кнопки всплываюшего окна
                    clickOpenoknoTytoknoall();

                //Вызываем всплывашку
                    $(".blockcentr").slideToggle("2000");
                    if($("div").hasClass("blockall"))
                        $(".blockall").remove();
                    else
                        $(".tytoknoall").append('<div class="blockall"></div>');

            }
        );


    }

/**
 * Событие по окончании загрузки страницы
 * */
    $(document).ready(function() {

        /**
        * Навешиваем события кликов на кнопки всплываюшего окна
        * */
            clickOpenoknoTytoknoall();

    });


/**
 * Событие на нажатие клавиш ctr + Enter
 * */
    $(document).keydown(function(e) {
        // с поддержкой macOS X
        if ((e.ctrlKey || e.metaKey) && (e.keyCode == 13 || e.keyCode == 10)) {
            ajaxYandexSpellchecking(); //отправляет аякс запрос в сервис Яндекс для проверки орфографии
        }
    });

