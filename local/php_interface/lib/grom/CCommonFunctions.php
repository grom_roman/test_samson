<?php

namespace Grom;

use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock as HL;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Service\GeoIp;
use \Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use \Bitrix\Main\Diag\Debug;

/*
 * Класс общих ф-й
 * */

class CCommonFunctions extends \CBitrixComponent
{

    /**
     * Возвращает ID инфоблока по его CODE
     * */
    public static function getIdIBlockByCode($codeIBlock) // \Grom\CCommonFunctions::getIdIBlockByCode(codeIBlock)
    {
        if (!\CModule::IncludeModule("iblock")) die('iblock module is not included!');

        $ibList = \CIBlock::GetList(
            Array(),
            Array(
                "CODE" => $codeIBlock,
                "CHECK_PERMISSIONS" => "N"
            )
        );
        while ($arRes = $ibList->Fetch()) {
            if ($arRes["ID"]) {
                return $arRes["ID"];
            } else {
                return '';
            }
        }

    }


    /**
     * Возвращает все элементы по CODE инфоблока и фильтру
     *
     * -------------------
     * $msFilter =
     * array(
     *      "LOGIC" => "OR",
     *      array("<PROPERTY_RADIUS" => 50, "=PROPERTY_CONDITION" => "Y"),
     *      array(">=PROPERTY_RADIUS" => 50, "!=PROPERTY_CONDITION" => "Y"),
     * )
     * ----------------------
     *
     * */
    public static function getAllElementsIbCodeFilter($codeIBlock, $arFilter=[], $arSel=[]) // \Grom\CCommonFunctions::getAllElementsIbCodeFilter($codeIBlock, $msFilter);
    {
        if (!\CModule::IncludeModule("iblock")) die('iblock module is not included!');
        if($codeIBlock && !$arFilter['IBLOCK_CODE']) {
            $arFilter['IBLOCK_CODE'] = $codeIBlock;
        }
        if ($codeIBlock) {
            $masElements = [];
            if(empty($arSel)) {
                $arSelect = Array("*","PROPERTY_*");
            } else {
                $arSelect = $arSel;
            }
            $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect); //Array("nPageSize"=>5000)
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                $masElements[$arFields['ID']] = $arFields;
            }

        return $masElements;
        }


    }


    /**
     * Вывод массивов на экран
     * Ограничение только для Админа нельзя использовать в init.php
     * */
    public static function pa($massPrint, $metka='', $adminOnly=false, $hide=false, $exit=false)  // \Grom\CCommonFunctions::pa($massPrint, $metka='', $adminOnly=false, $hide=false, $exit=false)
    {
        global $USER;

        if ($hide) {
            $display = 'display:none; ';
        }

        if($adminOnly) {
            if(is_object($USER)) {
                if ($USER->IsAdmin()) {
                    print_r($metka.'<pre style="'.$display.' background: white;">');
                    print_r($massPrint); print_r('<br/>');
                    print_r('</pre>');
                }
             } else {

            }
        } else {
            print_r($metka.'<pre style="'.$display.' background: white;">');
            print_r($massPrint); print_r('<br/>');
            print_r('</pre>');
        }

        if($exit) {
            exit();
        }

    }






}

