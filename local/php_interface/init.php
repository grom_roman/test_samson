<?

//Подключаемые классы
    \Bitrix\Main\Loader::registerAutoLoadClasses(
        null,
        array(
            'Grom\CCommonFunctions' => '/local/php_interface/lib/grom/CCommonFunctions.php',
            'Grom\CGlobalsIds' => '/local/php_interface/lib/grom/CGlobalsIds.php',
        )
    );

//Подключаем модули
    $includeModules = [
        'iblock',
        'highloadblock'
    ];

    foreach ($includeModules as $module){
        \Bitrix\Main\Loader::includeModule($module);
    }

