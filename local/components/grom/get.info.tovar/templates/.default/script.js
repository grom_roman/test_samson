



/**
 * Функция навешивания событий получения значения товара по коду путем отправки аякс запроса
 * на инпуты для ввода кода товара на странице addtovars
 * */
    function blurInputAddTovars(inputName) {

        //Удаляем события, так как у нас инпут копируется вместе с событием
        $( "#formAddTovars input[name=" + inputName + "]" ).off('blur');

        //Наввешиваем событие
        $( "#formAddTovars input[name=" + inputName + "]" ).blur(function(){
            let xmlId = $(this).val();
            //Отправляем аякс запрос
            ajaxGetInfoTovarXmlId(xmlId,inputName,'INFO');
        });
    }

/**
 * Функция отправляет аякс запрос в компонент для получения информации
 *
 * */
    function ajaxGetInfoTovarXmlId(xmlId,inputName,action) {

        let url = "/addtovars/index.php";

        $.post(
            url,
            {
                "INPUT_NAME" : inputName,
                "XML_ID" : xmlId,
                "ACTION" : action
            },
            function(data){

                //console.log(data);
                data = JSON.parse(data);

                if (data.status == "ok" || data.status == "error") {
                    if (data.status == "ok") {
                        let inputName = data.class;
                        $( "#formAddTovars span." + inputName + "" ).html(data.message);
                        $( "#formAddTovars span." + inputName + "" ).css('color','green');
                    }
                    if (data.status == "error") {
                        let inputName = data.class;
                        $( "#formAddTovars span." + inputName + "" ).html(data.message);
                        $( "#formAddTovars span." + inputName + "" ).css('color','red');
                    }
                }

                BX.onCustomEvent('OnBasketChange');

            }
        );


    }


/**
 * Событие по окончании загрузки страницы
 * */
$(document).ready(function() {


    /**
     * Добавить поля инпута
     * */
        var inputs = 1;
        $('#formAddTovars .btnAdd').click(function() {
            $('#formAddTovars .btnDel:disabled').removeAttr('disabled');
            var c = $('#formAddTovars .clonedInput:first').clone(true);
            let inputName = 'input' + (++inputs);
            c.children(':text').attr('name', inputName );
            c.children(':text').val('');
            c.children('span').removeClass('input1');
            c.children('span').addClass(inputName);
            c.children('span').html('');
            $('#formAddTovars .clonedInput:last').after(c);
            //Навешиваем событие потери фокуса на новые инпуты
            blurInputAddTovars(inputName);
        });
    /**
     * Удалить поля инпута
     * */
        $('#formAddTovars .btnDel').click(function() {
            --inputs;
            $(this).closest('.clonedInput').remove();
            $('#formAddTovars .btnDel').attr('disabled',($('#formAddTovars .clonedInput').length  < 2));
        });
    /**
     * Добавление товаров в корзину
     * */
        $('#formAddTovars .btnBasket').click(function() {

            let xmlId = $('#formAddTovars').serialize();
            let inputName = 'result-add-basket';
            //Отправляем аякс запрос
                ajaxGetInfoTovarXmlId(xmlId,inputName,'ADD_BASKET');

        });



    //Навешиваем событие потери фокуса на первый инпут
        blurInputAddTovars('input1');

});
