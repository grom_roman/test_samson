<?

namespace Grom;

use Bitrix\Main;
use Bitrix\Main\Application;

class GetInfoTovarComponent extends \CBitrixComponent
{

    /** @var Main\Server */
    private $server;

    /** @var Main\Request */
    private $req;


    /**
     * инициализация параметров
     */
    protected function initParams() : void
    {
        $this->server = \Bitrix\Main\Application::getInstance()->getContext()->getServer();
        $this->req = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    }



    /**
     * Получение записей ORM
     * @param int $limit
     * @param int $offset
     * @param int $sectionId
     * @return Query
     */
        public function getQueryIBElemXmlId(string $iblockCode, string $valXmlId)
        {

            //--------------------
            $entity =  \Bitrix\Iblock\IblockTable::compileEntity($iblockCode);

            //Переменные
            $iblockId = \Grom\CCommonFunctions::getIdIBlockByCode($iblockCode); //38;
            $entityWakeUp = \Bitrix\Iblock\Iblock::wakeUp($iblockId);
            $entityDataClass = $entityWakeUp->getEntityDataClass();

            //Запрос
            $query = $entityDataClass::query()

                ->registerRuntimeField('IBLOCK', array(
                    'data_type'=> \Bitrix\Iblock\IblockTable::class,
                    'reference' => [
                        '=this.IBLOCK_ID' => 'ref.ID'
                    ]
                ))

                ->where('IBLOCK_ID', $iblockId)
                ->where('CODE', $valXmlId)
                ->where('ACTIVE', 'Y')
                ->setSelect([
                    'ID', 'CODE', 'NAME', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'DATE_CREATE',
                    'DETAIL_PAGE' => 'IBLOCK.DETAIL_PAGE_URL',
                ])
            ;

            return $query;
        }



    /**
     * Возвращает массив Id товаров по их кодам вида "CODE" => "ID"
     * @return array
     */
    public function getProductFromCode($codeIBlock, $masCodes)
    {

        $msFilter = [];
        $msFilter = array(
            array("=CODE" => $masCodes),
        );
        $resTovars = \Grom\CCommonFunctions::getAllElementsIbCodeFilter($codeIBlock, $msFilter);
        $productIds = [];
        foreach($resTovars as $tovar) {
            $productIds[$tovar['CODE']] = $tovar['ID'];
        }

        return $productIds;

    }

    /**
     * Добавляет товары в корзину, возвращает массив коментаиев успешных и нет
     * @return array|mixed
     */
    public function addBasket($masTovarIds){

        \Bitrix\Main\Loader::includeModule("catalog");

        $masComment = [];
        foreach($masTovarIds as $tovarCode => $tovarId) {

            $fields = [
                'PRODUCT_ID' => intval($tovarId), // ID товара, обязательно
                'QUANTITY' => intval(1), // количество, обязательно
                'PROPS' => [
                    ['NAME' => 'basket_detail', 'CODE' => 'basket_detail', 'VALUE' => ''],
                ],
            ];

            $r = \Bitrix\Catalog\Product\Basket::addProduct($fields);

            if (!$r->isSuccess()) {
                $errors = $r->getErrorMessages();
                foreach($errors as $error) {
                    $masComment[$tovarId] = '<span style=\'color:red\'>Товар с (Code=' . $tovarCode .', Id=' . $tovarId . ') - ' . $error . '</span>';
                }
            } else {
                $masComment[$tovarId] = '<span style=\'color:green\'> Товар с (Code=' . $tovarCode .', Id=' . $tovarId . ') - Успешно добавлен</span>';
            }
        }

        return $masComment;
    }


     /**
     * @return array|mixed
     */
    public function executeComponent(){

        global $APPLICATION;

        $this->initParams();

        if($this->req->isPost()) {

            /**
             * Берем необходимые переменные
             * */
                $postParamXmlId = $this->req->getPost("XML_ID");
                $postParamInputName = $this->req->getPost("INPUT_NAME");
                $postParamAction = $this->req->getPost("ACTION");
                $codeIBlock = \Grom\CGlobalsIds::CONST_CODE_IB_CLOTHES;
                $codeIBlockOffers = \Grom\CGlobalsIds::CONST_CODE_IB_CLOTHES_OFFERS;


            /**
             * 1) Обработка Аякс запроса при снятии фокуса с инпута для получения информации по коду товара
             * 2) Обработка Аякс запроса Заполняем корзину
             * */

            if($postParamAction == 'INFO') {

                /**
                 * Обработка Аякс запроса при снятии фокуса с инпута для получения информации по коду товара
                 * */
                if($postParamXmlId) {

                    $valXmlId = $postParamXmlId;
                    $flagOffers = ' (товар) ';

                    //Делаем поиск по Инфоблоку товаров
                        $itemsEll = [];
                        $query = self::getQueryIBElemXmlId($codeIBlock, $valXmlId);
                        $rsItems = $query->exec();
                        while ($item = $rsItems->fetch()) {
                            $itemsEll[] = $item;
                        }
                    if(empty($itemsEll[0]['CODE'])) {
                        //Делаем поиск по Инфоблоку предложений
                        $itemsEll = [];
                        $flagOffers = ' (предложение) ';
                        $queryOffers = self::getQueryIBElemXmlId($codeIBlockOffers, $valXmlId);
                        $rsItems = $queryOffers->exec();
                        while ($item = $rsItems->fetch()) {
                            $itemsEll[] = $item;
                        }
                    }

                }

                //Формируем ответ
                    if(!empty($itemsEll[0]['CODE'])) {
                        $resultJson = '{"status":"' . 'ok' . '","message":"' . $itemsEll[0]['NAME'] . $flagOffers . '","class":"' . $postParamInputName . '"}';
                    } else {
                        $resultJson = '{"status":"' . 'error' . '","message":"Товар по данному коду не найден","class":"' . $postParamInputName . '"}';
                    }

                //Отправляем JSON
                    $APPLICATION->RestartBuffer();
                        echo $resultJson;
                    die();


            }
            elseif($postParamAction == 'ADD_BASKET') {

                /**
                 * Обработка Аякс запроса Заполняем корзину
                 * */

                //Парсим массив входных данных из инпутов
                    $masCodes = [];
                    if($postParamXmlId) {
                       foreach(explode('&', $postParamXmlId) as $line) {
                           $param = explode('=', $line);
                            if(!empty($param[1])) {
                                $masCodes[] = $param[1];
                            }
                       }
                    }

                //Получаем массив Id товаров по их кодам вида "CODE" => "ID" из инфоблока товаров
                    $masTovarIds = self::getProductFromCode($codeIBlock, $masCodes);

                //Получаем массив Id товаров по их кодам вида "CODE" => "ID" из инфоблока предложений
                    $masOffersIds = self::getProductFromCode($codeIBlockOffers, $masCodes);

                $masTovarIds = array_merge($masTovarIds,$masOffersIds);

                //Добавляем товары в корзину, возвращает массив коментаиев успешных и нет
                    $masReturn = self::addBasket($masTovarIds);
                    $messages = '';
                    foreach($masReturn as $idTovar => $mess) {
                        $messages .= $mess . '<br/>';
                    }

                //Формируем ответ
                    if(!empty($messages)) {
                        $resultJson = '{"status":"' . 'ok' . '","message":"' . $messages . '","class":"' . $postParamInputName . '"}';
                    } else {
                        $resultJson = '{"status":"' . 'error' . '","message":"Товары для добавления не найдены","class":"' . $postParamInputName . '"}';
                    }

                //Отправляем JSON
                    $APPLICATION->RestartBuffer();
                        echo $resultJson;
                    die();
            }


        } else {
            //Выводим шаблон
                $this->includeComponentTemplate();
        }


    }


}
?>