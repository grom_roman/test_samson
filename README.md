Поставить решение «Интернет-магазин» из дистрибутива «1С-Битрикс: Управление сайтом - Бизнес».

1. Реализовать сообщение об ошибке в тексте по нажатию Ctrl+Enter для выделенного фрагмента текста. Реализация произвольная.

2. Реализовать сравнение товаров. В карточке товара должен быть индикатор «уже в сравнении», отдельная страница со сравнением выбранных товаров.

3. Реализовать добавление товаров в корзину по коду товара (поле XML_ID). Множественные поля для ввода кодов с подтягиванием информации о товаре, возможность добавить/удалить поле.

